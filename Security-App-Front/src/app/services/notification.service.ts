import { Injectable } from '@angular/core';
import { NotifierService } from 'angular-notifier';

import { NotificationType } from '../enums/notification-type.enum';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private notifier: NotifierService) { }

  /**
	 * Show a notification
	 *
	 * @param {string} type    Notification type
	 * @param {string} message Notification message
	*/
	public notify( type: NotificationType, message: string ): void {
		this.notifier.notify( type, message );
	}
}
