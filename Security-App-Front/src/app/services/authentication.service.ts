import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { User } from '../model/user';

import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public host = environment.apiurl;
  private token: string;
  private logedInUsername: string;

  private jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient) {
    this.token = '';
    this.logedInUsername = '';
  }

  login(user: User): Observable<HttpResponse<User>> {
    let url = this.host + 'login';
    return this.http.post<User>(this.host + '/user/login', user, {observe: 'response'});
    // observe: reponse ---> give the hole reponse including everything (headers and everything)
  }


  register(user: User): Observable<User> {
    return this.http.post<User>(this.host + '/user/register', user);
  }


  logOut(): void {
    this.token = null;
    this.logedInUsername = null;
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    localStorage.removeItem('users');
  }

  addUserToLocalCache(user: User): void {
    let userToJSON = JSON.stringify(user);
    localStorage.setItem('user', userToJSON);
  }

  getUserFromLocalCache(): User  {
    let user = localStorage.getItem('user');
    if (user != null) {
      return JSON.parse(user);
    }
    return null;
  }

  loadToken(): void {
    this.token = localStorage.getItem('token');
  }

  saveToken(token: string): void {
    this.token = token;
    if (token) localStorage.setItem('token', token);
  }

  getToken(): string {
    return this.token;
  }

  isUserLoggedIn(): boolean {
    this.loadToken();
    if (this.token != null && this.token !== '') {
      if (this.jwtHelper.decodeToken(this.token).sub != null || '') {
        if (!this.jwtHelper.isTokenExpired(this.token)) {
          this.logedInUsername = this.jwtHelper.decodeToken(this.token).sub;
          return true;
        }
      }
    } else {
      this.logOut();
      return false;
    }
  }

}
