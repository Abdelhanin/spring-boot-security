import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpEvent, HttpResponse } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { CustomHttpResponse } from '../model/custom-http-response';


class CustomHttpRespone {
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private host = environment.apiurl;


  constructor(private http: HttpClient) {}


  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.host + '/user/list');
  }

  addUser(formData: FormData): Observable<User> {
    return this.http.post<User>(this.host + '/user/add', formData);
  }

  updateUser(formData: FormData): Observable<User> {
    return this.http.post<User>(this.host + '/user/update', formData);
  }

  resetPassword(email: string): Observable<CustomHttpResponse | HttpErrorResponse> {
    return this.http.get<CustomHttpResponse>(this.host + '/user/resetpassword/' + email);
  }

  updateProfileImage(formData: FormData): Observable<HttpEvent<User> | HttpErrorResponse> {
    return this.http.post<User>(this.host + '/user/updateProfileImage', formData, {reportProgress: true, observe: 'events'}); // we give a user some indication that in progress
  }

  public deleteUser(username: string): Observable<CustomHttpRespone> {
    return this.http.delete<CustomHttpRespone>(`${this.host}/user/delete/${username}`);
  }

  addUsersToLocalCache(users: User[]): void{
    localStorage.setItem('users', JSON.stringify(users));
  }

  getUsersFromLocalCache(): User[] | null{
    let users: string | null = localStorage.getItem('users');
    if (users != null) {
      return JSON.parse(users);
    }
    return null;
  }

  public createUserFormDate(loggedInUsername: string | null, user: User, profileImage: File): FormData {
    const formData = new FormData();
    if (loggedInUsername != null) formData.append('currentUsername', loggedInUsername);
    formData.append('firstName', user.firstName);
    formData.append('lastName', user.lastName);
    formData.append('username', user.username);
    formData.append('email', user.email);
    if (!user.role.includes("ROLE"))
      formData.append('role', "ROLE_" + user.role);
    else
      formData.append('role', user.role);

    formData.append('profileImage', profileImage);
    formData.append('isActive', JSON.stringify(user.active));
    formData.append('isNonLocked', JSON.stringify(user.notLocked));
    return formData;
  }

}
