import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, Pipe } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { HeaderType } from 'src/app/enums/header-type.enum';
import { NotificationType } from 'src/app/enums/notification-type.enum';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/notification.service';

import { delay } from 'rxjs/operators'



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  public showLoading = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService) {}



    ngOnInit(): void {
      if (this.authenticationService.isUserLoggedIn()) {
        this.router.navigateByUrl('/user/management');
      } else {
        this.router.navigateByUrl('/login');
      }
    }

    ngOnDestroy(): void {
      this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    onLogin(user: User): void {
      this.showLoading = true;
      this.subscriptions.push(
        this.authenticationService.login(user).pipe().subscribe((response: HttpResponse<User>) => {
          console.log(response);
          const token = response.headers.get(HeaderType.JWT_TOKEN);
          this.authenticationService.saveToken(token);
          if (response?.body) this.authenticationService.addUserToLocalCache(response.body);
          this.router.navigateByUrl('/user/management');
          this.showLoading = false;
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendErrorNotification(NotificationType.ERROR, errorResponse.error.message);
          this.showLoading = false;
        })
      );
    }

    private sendErrorNotification(notificationType: NotificationType, message: string): void {
      message ? this.notificationService.notify(notificationType, message) :
        this.notificationService.notify(notificationType, 'An error occurred. Please try again.');
    }
}
